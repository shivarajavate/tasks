﻿using Tasks.ViewModels;
using Xamarin.Forms;

namespace Tasks.Views
{
    public partial class TaskDetailsPage : ContentPage
    {
        public TaskDetailsPage()
        {
            InitializeComponent();
            BindingContext = new TaskDetailsViewModel();
        }
    }
}