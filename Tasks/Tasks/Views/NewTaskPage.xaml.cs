﻿using Tasks.Models;
using Tasks.ViewModels;
using Xamarin.Forms;

namespace Tasks.Views
{
    public partial class NewTaskPage : ContentPage
    {
        public Task Task { get; set; }

        public NewTaskPage()
        {
            InitializeComponent();
            BindingContext = new NewTaskViewModel();
        }
    }
}