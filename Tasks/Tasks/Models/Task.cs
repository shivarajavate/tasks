﻿namespace Tasks.Models
{
    public class Task
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}
