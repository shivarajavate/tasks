﻿using Tasks.ViewModels;
using Tasks.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Tasks
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(TaskDetailsPage), typeof(TaskDetailsPage));
            Routing.RegisterRoute(nameof(NewTaskPage), typeof(NewTaskPage));
        }

    }
}
