﻿using System.Collections.Generic;

namespace Tasks.Services
{
    public interface IDataStore<T>
    {
        bool AddTask(T task);
        T GetTask(string id);
        IEnumerable<T> GetTasks();
        bool UpdateTask(T task);
        bool DeleteTask(string id);
    }
}
