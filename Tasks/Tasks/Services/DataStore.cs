﻿using Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks.Services
{
    public class DataStore : IDataStore<Task>
    {
        private List<Task> tasks;
        public DataStore()
        {
            tasks = new List<Task>()
            {
                new Task { Id = Guid.NewGuid().ToString(), Text = "Sample Task", Description="This is a sample Task." }
            };
        }
        public bool AddTask(Task item)
        {
            tasks.Add(item);
            return true;
        }
        public Task GetTask(string id)
        {
            return tasks.FirstOrDefault(task => task.Id == id);
        }
        public IEnumerable<Task> GetTasks()
        {
            return tasks;
        }
        public bool UpdateTask(Task currentTask)
        {
            var oldTask = tasks.Where(task => task.Id == currentTask.Id).FirstOrDefault();
            tasks.Remove(oldTask);
            tasks.Add(currentTask);
            return true;
        }
        public bool DeleteTask(string id)
        {
            var oldTask = tasks.Where(task => task.Id == id).FirstOrDefault();
            tasks.Remove(oldTask);
            return true;
        }
    }
}