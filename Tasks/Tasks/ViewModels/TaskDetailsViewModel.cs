﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace Tasks.ViewModels
{
    [QueryProperty(nameof(TaskId), nameof(TaskId))]
    public class TaskDetailsViewModel : BaseViewModel
    {
        private string taskId;
        private string text;
        private string description;
        public string Id { get; set; }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public string TaskId
        {
            get
            {
                return taskId;
            }
            set
            {
                taskId = value;
                LoadTaskId(value);
            }
        }

        public void LoadTaskId(string taskId)
        {
            try
            {
                var task = DataStore.GetTask(taskId);
                Id = task.Id;
                Text = task.Text;
                Description = task.Description;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Task");
            }
        }
    }
}
