﻿using Tasks.Models;
using Tasks.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace Tasks.ViewModels
{
    public class TasksViewModel : BaseViewModel
    {
        public ObservableCollection<Task> Tasks { get; }
        public Command LoadTasksCommand { get; }
        public Command AddTaskCommand { get; }
        public Command<Task> ViewTaskCommand { get; }
        public Command<Task> DeleteTaskCommand { get; }
        public TasksViewModel()
        {
            Title = "Tasks";
            Tasks = new ObservableCollection<Task>();
            LoadTasksCommand = new Command(OnLoadTasks);
            AddTaskCommand = new Command(OnAddTask);
            ViewTaskCommand = new Command<Task>(OnTaskSelected);
            DeleteTaskCommand = new Command<Task>(OnDeleteTask);
            OnLoadTasks();
        }
        void OnLoadTasks()
        {
            IsBusy = true;
            try
            {
                Tasks.Clear();
                var tasks = DataStore.GetTasks();
                foreach (var task in tasks)
                {
                    Tasks.Add(task);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public void OnAppearing()
        {
            IsBusy = true;
        }
        private async void OnAddTask(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewTaskPage));
        }    
        async void OnTaskSelected(Task task)
        {
            if (task == null)
                return;
            // This will push the TaskDetailsPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(TaskDetailsPage)}?{nameof(TaskDetailsViewModel.TaskId)}={task.Id}");
        }
        private void OnDeleteTask(Task task)
        {
            Tasks.Remove(task);
            DataStore.DeleteTask(task.Id);
        }
    }
}